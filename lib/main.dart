import 'package:flutter/material.dart';

//********************************* CLASS MAIN ********************************* */
void main() => runApp(new MyApp());

//********************************* CLASS MY APP ******************************* */
class MyApp extends StatelessWidget {

  //*************************** WIDGET ROOT **************************** */
  @override
  Widget build(BuildContext context)
  {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
      ),
      home: new Home(),
      builder: (BuildContext context,Widget child){
        return Padding(
          child: child,
          padding: EdgeInsets.only(bottom: 50.0),
        );
      },
    );
  }
}

//************************************** CLASS HOME **************************************** */
class Home extends StatefulWidget {

  //::::::::::::::::::::::::: CALL STATE ::::::::::::::::::::::::::::
  @override
  _HomeState createState() => _HomeState();
}

//************************************** CLASS STATE HOME ********************************** */
class _HomeState extends State<Home> {

  //************************* WIDGET ROOT ******************************** */
  @override
  Widget build(BuildContext context) {
    //::::::::::::::::::::::::::::::: STACTK :::::::::::::::::::::::::::
    return Stack(
      children: <Widget>[
        //::::::::::::::::::::: CONTAINER - PICTURE ::::::::::::::::::::
        new Container(
          height: double.infinity,
          width: double.infinity,
          decoration:new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("images/xxx.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),

        //::::::::::::::::::::::: SCAFFOLD - APP AND BODY :::::::::::::::
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: new AppBar(
            title: new Text("csd"),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          body: new Container(
            color: Colors.blue,

          ),
        ),

      ],
    );
  }
}
//**************************************************************************************** */